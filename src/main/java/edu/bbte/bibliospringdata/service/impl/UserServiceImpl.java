package edu.bbte.bibliospringdata.service.impl;

import edu.bbte.bibliospringdata.model.User;
import edu.bbte.bibliospringdata.repository.UserRepository;
import edu.bbte.bibliospringdata.service.UserService;
import edu.bbte.bibliospringdata.util.PasswordEncrypter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private PasswordEncrypter passwordEncrypter;

    @Override
    public User create(User user) {
        if (user.getPassword() != null) {
            String encryptedPassword = passwordEncrypter.generateHashedPassword(user.getPassword(), user.getUuid());
            user.setPassword(encryptedPassword);
        }
        return userRepository.saveAndFlush(user);
    }


    @Override
    public boolean login(User user) {
        User dbUser = userRepository.findByUsername(user.getUsername());
        if (dbUser != null) {
            user.setPassword(passwordEncrypter.generateHashedPassword(user.getPassword(), dbUser.getUuid()));
            return dbUser.getPassword().equals(user.getPassword());
        } else {
            return false;
        }
    }


    @Override
    public User update(User user) {
        return userRepository.saveAndFlush(user);
    }

    @Override
    public boolean deleteById(Long id) {
        if (userRepository.existsById(id)) {
            userRepository.deleteById(id);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll();
    }

    @Override
    public User getById(Long id) {
        return userRepository.findById(id).orElse(null);

    }
}
