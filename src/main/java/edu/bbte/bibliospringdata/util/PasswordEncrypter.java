package edu.bbte.bibliospringdata.util;

import org.springframework.stereotype.Component;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

@Component
public class PasswordEncrypter {

    public String generateHashedPassword(String password, String salt) {
        byte[] input = (password + salt).getBytes();
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            messageDigest.reset();
            messageDigest.update(input);


            byte[] output = messageDigest.digest();
            StringBuilder stringBuilder = new StringBuilder();

            for (byte character : output) {
                stringBuilder.append(String.format("%02x", character));
            }
            return stringBuilder.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new IllegalStateException("SHA-256 algorithm not found", e);
        }
    }
}
