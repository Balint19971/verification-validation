package edu.bbte.bibliospringdata.model;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class BookTest {

    @Test
    void setGetAndAddAuthorsTest() {
        Author author1 = new Author();
        author1.setName("Valaki");
        Author author2 = new Author();
        author2.setName("valami");
        Author author3 = new Author();
        author3.setName("akarmi");

        List<Author> authors = new ArrayList<>();
        authors.add(author1);
        authors.add(author2);

        Book book = new Book("Harry Potter", 400, "1997.03.10");
        book.setAuthors(authors);
        book.addAuthor(author3);

        assertEquals(book.getAuthors().size(), 3, "model book setGetAndAddAuthorsTest failed");
    }
}
