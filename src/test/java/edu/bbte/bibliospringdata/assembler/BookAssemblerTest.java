package edu.bbte.bibliospringdata.assembler;

import edu.bbte.bibliospringdata.dto.incoming.BookInDTO;
import edu.bbte.bibliospringdata.dto.outcoming.BookOutDTO;
import edu.bbte.bibliospringdata.model.Author;
import edu.bbte.bibliospringdata.model.Book;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class BookAssemblerTest {

    private BookInDTO bookInDTO;
    private Book book;

    @Autowired
    private BookAssembler bookAssembler;

    @BeforeEach
    void init() {
        bookInDTO = new BookInDTO();
        bookInDTO.setId(1L);
        bookInDTO.setTitle("Harry Potter");
        bookInDTO.setPageNumber(400);
        bookInDTO.setPublishingDate("1997.03.10");
        bookInDTO.setAuthorsNames("Valaki fasf, valami sdfasf, akarmi");

        Author author1 = new Author();
        author1.setName("Valaki");
        Author author2 = new Author();
        author2.setName("valami");
        Author author3 = new Author();
        author3.setName("akarmi");

        book = new Book("Harry Potter", 400, "1997.03.10");
        book.setUuid("alksdjfljaslfjasldfjaslkdjfalksjdflasldkfj");
        book.setId(1L);
        book.addAuthor(author1);
        book.addAuthor(author2);
        book.addAuthor(author3);
    }

    @Test
    void bookInDTOToBookIdTest() {
        Book assambledBook = bookAssembler.bookInDTOToBook(bookInDTO);

        assertEquals(assambledBook.getId(), 1, "Id do not match after bookInDTOToBook");
    }

    @Test
    void bookInDTOToBookTitleTest() {
        Book assambledBook = bookAssembler.bookInDTOToBook(bookInDTO);

        assertEquals(assambledBook.getTitle(), "Harry Potter", "Title do not match after"
                + " bookInDTOToBook");

    }

    @Test
    void bookInDTOToBookPageNumberTest() {
        Book assambledBook = bookAssembler.bookInDTOToBook(bookInDTO);

        assertEquals(assambledBook.getPageNumber(), 400, "PageNumber do not match after"
                + " bookInDTOToBook");

    }

    @Test
    void bookInDTOToBookPublishingDateTest() {
        Book assambledBook = bookAssembler.bookInDTOToBook(bookInDTO);

        assertEquals(assambledBook.getPublishingDate(), "1997.03.10", "PublishingDate do not match after"
                + "bookInDTOToBook");


    }

    @Test
    void bookInDTOToBookAuthorSizeTest() {
        Book assambledBook = bookAssembler.bookInDTOToBook(bookInDTO);

        assertEquals(assambledBook.getAuthors().size(), 3, "AuthorSize do not match after"
                + " bookInDTOToBook");

    }

    @Test
    void bookToBookOutDTOUuidTest() {
        BookOutDTO assambledBookOutDTO = bookAssembler.bookToBookOutDTO(book);

        assertEquals(assambledBookOutDTO.getUuid(), book.getUuid(), "Uuid do not match after bookToBookOutDTO");
    }

    @Test
    void bookToBookOutDTOIdTest() {
        BookOutDTO assambledBookOutDTO = bookAssembler.bookToBookOutDTO(book);

        assertEquals(assambledBookOutDTO.getId(), book.getId(), "Id do not match after bookToBookOutDTO");
    }

    @Test
    void bookToBookOutDTOTitleTest() {
        BookOutDTO assambledBookOutDTO = bookAssembler.bookToBookOutDTO(book);

        assertEquals(assambledBookOutDTO.getTitle(), book.getTitle(), "Title do not match after"
                + " bookToBookOutDTO");
    }

    @Test
    void bookToBookOutDTOPageNumberTest() {
        BookOutDTO assambledBookOutDTO = bookAssembler.bookToBookOutDTO(book);

        assertEquals(assambledBookOutDTO.getPageNumber(), book.getPageNumber(), "PageNumber do not match after"
                + " bookToBookOutDTO");
    }

    @Test
    void bookToBookOutDTOPublishingDateTest() {
        BookOutDTO assambledBookOutDTO = bookAssembler.bookToBookOutDTO(book);

        assertEquals(assambledBookOutDTO.getPublishingDate(), book.getPublishingDate(), "PublishingDate do not"
                + " match after bookToBookOutDTO");
    }

    @Test
    void bookToBookOutDTOAuthorsNamesTest() {
        BookOutDTO assambledBookOutDTO = bookAssembler.bookToBookOutDTO(book);

        assertEquals(assambledBookOutDTO.getAuthorsNames(), "Valaki, valami, akarmi", "AuthrosNames"
                + " do not match after bookToBookOutDTO");
    }
}
