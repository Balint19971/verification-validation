package edu.bbte.bibliospringdata.assembler;

import edu.bbte.bibliospringdata.dto.incoming.UserInDTO;
import edu.bbte.bibliospringdata.dto.outcoming.UserOutDTO;
import edu.bbte.bibliospringdata.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
class UserAssemblerTest {

    private UserInDTO userInDTO;

    private User user;

    @Autowired
    private UserAssembler userAssembler;

    @BeforeEach
    void inti() {
        userInDTO = new UserInDTO();
        userInDTO.setUsername("Bali");
        userInDTO.setPassword("valami");

        user = new User();
        user.setUuid("alksdjflasjdflasjdf");
        user.setId(1L);
        user.setUsername("Bali");
    }

    @Test
    void userInDTOToUserUsernameTest() {
        User user = userAssembler.userInDTOToUser(userInDTO);

        assertEquals(userInDTO.getUsername(), user.getUsername(), "Username do not match after userInDTOToUser");
    }

    @Test
    void userInDTOToUserPasswordTest() {
        User user = userAssembler.userInDTOToUser(userInDTO);

        assertEquals(userInDTO.getPassword(), user.getPassword(), "Password do not match after userInDTOToUser");

    }

    @Test
    void userToUserOutDTOIdTest() {
        UserOutDTO userOutDTO = userAssembler.userToUserOutDTO(user);

        assertEquals(user.getId(), userOutDTO.getId(), "IDs do not match after userToUserOutDTO");
    }

    @Test
    void userToUserOutDTOUuidTest() {
        UserOutDTO userOutDTO = userAssembler.userToUserOutDTO(user);

        assertEquals(user.getUuid(), userOutDTO.getUuid(), "Uuid do not match after userToUserOutDTO");
    }

    @Test
    void userToUserOutDTOUsernameTest() {
        UserOutDTO userOutDTO = userAssembler.userToUserOutDTO(user);

        assertEquals(user.getUsername(), userOutDTO.getUsername(), "Username do not match after userToUserOutDTO");
    }
}
