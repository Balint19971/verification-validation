package edu.bbte.bibliospringdata.service;

import edu.bbte.bibliospringdata.model.User;
import edu.bbte.bibliospringdata.repository.UserRepository;
import edu.bbte.bibliospringdata.util.PasswordEncrypter;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.BDDMockito.*;

@SpringBootTest
class MockedUserServiceTest {

    private User dbUser;
    private User loggedInUser;
    private User wrongPasswordUser;
    private User wrongUsernameUser;

    @Autowired
    private UserService userService;

    @MockBean
    private UserRepository mockedUserRepository;

    @MockBean
    private PasswordEncrypter passwordEncrypter;


    @BeforeEach
    void setup() {
        dbUser = new User();
        dbUser.setId(1L);
        dbUser.setUsername("Ubulka");
        dbUser.setPassword("ubulka");

        loggedInUser = new User();
        loggedInUser.setUsername("Ubulka");
        loggedInUser.setPassword("ubulka");

        wrongPasswordUser = new User();
        wrongPasswordUser.setUsername("Ubulka");
        wrongPasswordUser.setPassword("helytelen");

        wrongUsernameUser = new User();
        wrongUsernameUser.setUsername("helytelen");
        wrongUsernameUser.setPassword("ubulka");

    }

    @Test
    void testLoginExistingUser() {
        //given
        given(mockedUserRepository.findByUsername("Ubulka")).willReturn(dbUser);
        given(passwordEncrypter.generateHashedPassword(eq("ubulka"), anyString())).willReturn("ubulka");

        //when
        boolean existingUser = userService.login(loggedInUser);

        //then
        assertTrue(existingUser, "Existing user login failed");
    }

    @Test
    void testLoginWrongPasswordUser() {
        //given
        given(mockedUserRepository.findByUsername("Ubulka")).willReturn(dbUser);
        given(passwordEncrypter.generateHashedPassword(eq("ubulka"), anyString())).willReturn("ubulka");

        //when
        boolean wrongPassword = userService.login(wrongPasswordUser);

        //then
        assertFalse(wrongPassword, "wrong password user can login");
    }

    @Test
    void testLoginWrongUsernameUser() {
        given(mockedUserRepository.findByUsername("Ubulka")).willReturn(dbUser);
        given(passwordEncrypter.generateHashedPassword(eq("ubulka"), anyString())).willReturn("ubulka");

        boolean wrongUsername = userService.login(wrongUsernameUser);

        assertFalse(wrongUsername, "wrong username user can login");
    }

    @Test
    void testDeleteExistingUser() {
        given(mockedUserRepository.existsById(1L)).willReturn(true);
        given(mockedUserRepository.findById(1L)).willReturn(Optional.ofNullable(dbUser));

        boolean existingUser = userService.deleteById(dbUser.getId());

        assertTrue(existingUser, "existing user deletion failed");
    }

    @Test
    void testDeleteNonExistingUser() {
        given(mockedUserRepository.existsById(1L)).willReturn(true);
        given(mockedUserRepository.findById(1L)).willReturn(Optional.ofNullable(dbUser));


        boolean nonExistingUser = userService.deleteById(999L);

        assertFalse(nonExistingUser, "delet non existing user");
    }


}
