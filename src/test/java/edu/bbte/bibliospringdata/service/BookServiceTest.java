package edu.bbte.bibliospringdata.service;

import edu.bbte.bibliospringdata.model.Book;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class BookServiceTest {

    @Autowired
    private BookService bookService;

    @Test
    void createBookTest() {
        Book book = new Book("Avatar", 123, "10.03.2021");
        Book createdBook = bookService.create(book);
        assertNotNull(createdBook, "Book creation failed");
    }

    @Test
    void createBookWithSameTitleTest() {
        Book dbBook = bookService.getAll().get(0);
        int bookSizeBefore = bookService.getAll().size();
        bookService.create(dbBook);
        int bookSizeAfger = bookService.getAll().size();

        assertEquals(bookSizeBefore, bookSizeAfger, "Book with the same number was asserted");
    }
}
