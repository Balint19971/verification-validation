package edu.bbte.bibliospringdata.service;

import edu.bbte.bibliospringdata.model.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UserServiceTest {

    @Autowired
    private UserService userService;

    @Test
    void updateUserTest() {
        User nonExistiongUser = new User();
        nonExistiongUser.setUsername("Balint");
        nonExistiongUser.setPassword("valami");

        User createdUser = userService.update(nonExistiongUser);
        User existingUser = userService.getById(createdUser.getId());
        existingUser.setUsername("szilvi");
        User updatedUser = userService.update(existingUser);

        assertEquals(createdUser.getId(), updatedUser.getId(), "Updated user is not the same");
    }
}
