package edu.bbte.bibliospringdata.util;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class PasswordEncrypterTest {

    @Autowired
    PasswordEncrypter passwordEncrypter;

    @Test
    void testPasswordHashing() {
        String password = "password";
        String salt = "alma";
        String expected = "f4ac810b9e26403e54c6bc9dd45e9749d318a6ed1870e6b89b28338c7d35ee5b";

        String result = passwordEncrypter.generateHashedPassword(password, salt);
        assertEquals(expected, result, "password encrypter don t function well");
    }
}
