# FROM  gradle:8.5.0-jdk17-jammy
# WORKDIR /workdir
# COPY . ./
# RUN gradle build -x check
# CMD gradle bootRun

FROM eclipse-temurin:17.0.9_9-jre-jammy
COPY build/libs/bibliospring-data-*.jar ./
CMD java -jar bibliospring-data-*.jar